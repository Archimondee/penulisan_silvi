import React, { Component } from 'react';
import {  View, Text, } from 'react-native';
import {
    Content,
    Container,
    Card,
    CardItem,
} from 'native-base';
import {Ionicons} from '@expo/vector-icons';


export default class NilaiScreen extends Component {
    constructor(props){
        super(props);
        this.state ={
            nm_ast: '',
            nilai_praktikum: [],
            id_ast: this.props.navigation.getParam('id_asisten'),
            npm_mhs: this.props.navigation.getParam('npm_mhs'),
            id_praktikum: this.props.navigation.getParam('id_praktikum')
        }
    }
    static navigationOptions = ({ navigation }) => ({
        title: 'Nilai '+ navigation.getParam('id_praktikum'),
        //headerLeft: (<Button title="Back" onPress={() => { this.props.navigation.goBack() }} />),
        headerLeft: (<Ionicons name='ios-arrow-back' size={30} style={{ paddingLeft: 12 }} onPress={() => navigation.goBack()} />),
        headerStyle: { backgroundColor: '#08AF9E' }
    });
    componentDidMount=()=>{
        const {id_ast} = this.state;
        fetch('http://api-labti.000webhostapp.com/api/GetName.php',{
            method: 'POST',
            headers: {
                'Accept' : 'application/json',
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                id_ast: id_ast
            })
        }).then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    nm_ast: responseJson[0].nm_ast
                });
            }).catch((error) => {
                console.error(error);
        });

        const { npm_mhs } = this.state;
        const { id_praktikum } = this.state;

        fetch('http://api-labti.000webhostapp.com/api/ShowOnePelajaran.php', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                npm_mhs: npm_mhs,
                id_praktikum: id_praktikum
            })
        }).then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    nilai_praktikum: responseJson
                })
            }).catch((error) => {
                console.error(error);
            });
        
    }
    componentWillUpdate=()=>{
        
    }
    
    
  render() {
      const datas = this.state.nilai_praktikum;
    //   const Model = [
    //         {
    //             "header": "Pertemuan",
    //             "name" : "pert"
    //         },{
    //             "header": "Tugas Pendahuluan",
    //             "name" : "nilai_tp"
    //         },{
    //             "header": "Laporan pendahuluan",
    //             "name" : "nilai_lp"
    //         },{
    //             "header" : "Laporan Akhir",
    //             "name": "nilai_la"
    //         },{
    //             "header" : "Keaktifan",
    //             "name" : "nilai_k"
    //         }
    //     ];

      //console.log(datas);
      //console.log(this.state.nilai_praktikum);
    //   const renderNilai = datas.length>0?(
    //     // <Card transparent
    //     //     dataArray={datas}
    //     //     renderHeader={(datas)=>
    //     //         <CardItem header style={{borderBottomWidth:0.5}}>
    //     //             <Text>Pertemuan {} </Text>
    //     //         </CardItem>
    //     //     }
    //     //     renderRow={(datas)=>
    //     //         <CardItem cardBody style={{ flexDirection: 'column', alignItems: 'flex-start' }}>
    //     //             <Text>Nilai Tugas Pendahuluan : {datas.nilai_tp}</Text>
    //     //             <Text>Nilai Laporan Pendahuluan : {datas.nilai_lp}</Text>
    //     //             <Text>Nilai Laporan Akhir : {datas.nilai_la}</Text>
    //     //             <Text>Nilai Keaktifan : {datas.nilai_k}</Text>
    //     //         </CardItem>
    //     //     }
    //     //     renderFooter={(datas)=>
    //     //         <CardItem footer >
    //     //             <Text>Total : {}</Text>
    //     //         </CardItem>
    //     //     }
    //     // >
        
    //     // </Card>
    //       this.state.nilai_praktikum.map((items, i)=>{

    //       })
    //   ):(
    //       null
    //   )
      
    return (
      <Container>
        <Content padder>
            <Card transparent style={{flex:1}}>
                <CardItem header style={{borderWidth:1,backgroundColor:'#08AF9E',justifyContent:'center', alignContent:'center'}}>
                    <Text>Informasi Praktikum</Text>
                </CardItem>
                <CardItem style={{flexDirection:'column', alignItems:'flex-start'}}>
                    <Text> Nama Praktikum : {this.props.navigation.getParam('nm_praktikum')}</Text>
                    <Text> Kelas Praktikum : {this.props.navigation.getParam('kls_praktikum')} </Text>
                    <Text> Penanggung Jawab : {this.state.nm_ast}</Text>
                </CardItem>

                <CardItem header style={{marginTop:20, borderWidth:1,backgroundColor:'#08AF9E',justifyContent:'center', alignContent:'center'}}><Text>Nilai Praktikum</Text></CardItem>
                    <View style={{height:"100%", width:"95%", marginLeft:10, marginRight:10, marginBottom:20}}>
                        <View style={{flexDirection:'row', marginTop:10, borderWidth:1}}>
                            <View style={{flexWrap:'wrap', height:50, width:70, justifyContent:'center'}}>
                                <Text style={{textAlign:'center'}}> Pertemuan </Text>
                            </View>
                        
                            <View style={{flexWrap:'wrap', height:50, width:70, borderLeftWidth:1, justifyContent:'center'}}>
                                <Text style={{textAlign:'center'}}> Nilai LA </Text>
                            </View>
                        
                            <View style={{flexWrap:'wrap', height:50, width:70, borderLeftWidth:1, justifyContent:'center'}}>
                                <Text style={{textAlign:'center'}}> Nilai LP </Text>
                            </View>
                        
                            <View style={{flexWrap:'wrap', height:50, width:70, borderLeftWidth:1, justifyContent:'center'}}>
                                <Text style={{textAlign:'center'}}> Nilai TP </Text>
                            </View>
                        
                            <View style={{flexWrap:'wrap', height:50, width:70, borderLeftWidth:1, justifyContent:'center'}}>
                                <Text style={{textAlign:'center'}}> Nilai K </Text>
                            </View>
                        </View>
                        <View>
                            <View style={{flexDirection: 'column'}}>
                                {
                                    this.state.nilai_praktikum.map((items, i) => {
                                    return (
                                    <View  key={i} style={{flexDirection: 'row'}}>
                                        <View
                                        style={{
                                            height: 50,
                                            width: 70,
                                            
                                            justifyContent: 'center',
                                            borderLeftWidth:1,
                                            borderBottomWidth:1,
                                        }}
                                        >
                                        <Text style={{textAlign: 'center'}}> {items.pert} </Text>
                                        </View>
                                        <View
                                        style={{
                                            height: 50,
                                            width: 70,
                                            
                                            justifyContent: 'center',
                                            borderLeftWidth:1,
                                            borderBottomWidth:1,
                                        }}
                                        >
                                        <Text style={{textAlign: 'center'}}> {items.nilai_la} </Text>
                                        </View>
                                        <View
                                        style={{
                                            height: 50,
                                            width: 70,
                                            
                                            justifyContent: 'center',
                                            borderLeftWidth:1,
                                            borderBottomWidth:1,
                                        }}
                                        >
                                        <Text style={{textAlign: 'center'}}> {items.nilai_lp} </Text>
                                        </View>
                                        <View
                                        style={{
                                            height: 50,
                                            width: 70,
                                            
                                            justifyContent: 'center',
                                            borderLeftWidth:1,
                                            borderBottomWidth:1,
                                        }}
                                        >
                                        <Text style={{textAlign: 'center'}}> {items.nilai_tp} </Text>
                                        </View>
                                        <View
                                        style={{
                                            height: 50,
                                            width: 70,
                                        
                                            justifyContent: 'center',
                                            borderLeftWidth:1,
                                            borderBottomWidth:1,
                                            borderRightWidth:1
                                        }}
                                        >
                                        <Text style={{textAlign: 'center'}}> {items.nilai_k} </Text>
                                        </View>
                                    </View>
                                    );
                                })}
                                </View>

                        </View>
                </View>
            </Card>
        </Content>
      </Container>
    );
  }
}
