import React, { Component } from 'react';
import {  View, Text, StyleSheet, Image, ScrollView, AsyncStorage} from 'react-native';
import {
  Container,
  Header,
  Content,
  Label,
  Card,
  CardItem,
  Body,
  Left,
  Right,
  Button,
  Title,
} from 'native-base';
import {Ionicons} from '@expo/vector-icons';

export default class ProfileScreen extends Component {
    constructor(props){
      super(props);
      this.state = {
        data: [],
        username: '',
        npm: '',
        kelas: '',
        praktikum:[]
      }

    }

  static navigationOptions = ({navigation})=> ({
    title: 'Profile',
    headerStyle:{backgroundColor:'#08AF9E'}
  });
  
  
  componentDidMount (){
    AsyncStorage.getItem('username').then((value)=>{
      const username = value;
      fetch('http://api-labti.000webhostapp.com/api/GetProfile.php',{
      method: 'POST',
      headers: {
        'Accept' : 'application/json',
        'Content-Type' : 'application/json'
      },
      body: JSON.stringify({
        username_mhs: username
      })
    }).then((response)=>response.json())
        .then((responseJson)=>{
          this.setState({
            data: responseJson,
            npm: responseJson[0].npm_mhs,
            kelas: responseJson[0].kls_praktikum
          });
          this._saveData();
        }).catch((error)=>{
          console.error(error);
        });
    })
  }
  _saveData(){
    const data = this.state.data;
    AsyncStorage.setItem('Profile', JSON.stringify(data));
  }
   componentDidUpdate(){
      const npm = this.state.npm;
      const kelas = this.state.kelas;
      return fetch('https://api-labti.000webhostapp.com/api/ShowOnePraktikum.php',{
        method: 'POST',
        headers: {
          'Accept' : 'application/json',
          'Content-Type' : 'application/json'
        },
        body: JSON.stringify({
          npm_mhs: npm
        })
      }).then((response)=>response.json())
          .then((responseJson)=>{
            this.setState({
              praktikum: responseJson
            });
        }).catch((error)=>{
          console.error(error);
        });
   }

   renderTingkat1() {
    const AllData = this.state.praktikum;
    const tingkat1 = [];
    //console.log("Data = "+this.state.praktikum);
    AllData.map((items) => {
      if (items.tingkat === '1' ) {
        tingkat1.push(
          items,
        )
      }
    });
    //console.log(tingkat1);
    return tingkat1;
  }

  renderTingkat2() {
    const AllData = this.state.praktikum;
    const tingkat2 = [];
    AllData.map((items) => {
      if (items.tingkat === '2' ) {
        tingkat2.push(
          items,
        )
      }
    });
    //console.log(tingkat1);
    return tingkat2;
  }

  renderTingkat3() {
    const AllData = this.state.praktikum;
    const tingkat3 = [];
    AllData.map((items) => {
      if (items.tingkat === '3' ) {
        tingkat3.push(
          items,
        )
      }
    });
    //console.log(tingkat1);
    return tingkat3;
  }

  renderTingkat4() {
    const AllData = this.state.praktikum;
    const tingkat4 = [];
    AllData.map((items) => {
      if (items.tingkat === '4' ) {
        tingkat4.push(
          items,
        )
      }
    });
    //console.log(tingkat1);
    return tingkat4;
  }


  render() {
    //Rendering Profile
    //const profile = this.renderProfile();
    const datas = this.state.data;
    const renderProfile = datas.length>0?(
      <Card dataArray={datas} style={{ marginTop: 15 }}
       renderRow = {(datas)=>
        <CardItem transparent style={{}}>
          <Body style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', alignSelf: 'center' }}>
            <Text> Nama : {datas.nama_mhs}</Text>
            <Text> NPM : {datas.npm_mhs} </Text>
            <Text> Kelas : {datas.kls_praktikum} </Text>
          </Body>
        </CardItem>
       }>
      </Card>
    ):(
      null
    );

    const tingkatSatu = this.renderTingkat1 ();
    const CardSatu = tingkatSatu.length > 0
      ? <Card
          renderHeader={() => (
            <CardItem
              header
              style={{
                backgroundColor: '#08AF9E',
                borderWidth: 0.5,
                borderColor: 'gray',
              }}
            >
              <Text style={{color: 'black'}}> Tingkat 1</Text>
            </CardItem>
          )}
          dataArray={tingkatSatu}
          renderRow={tingkatSatu => (
            <CardItem
              button
              onPress={() =>
                this.props.navigation.navigate ('Nilai', {
                  npm_mhs: tingkatSatu.npm_mhs,
                  id_praktikum: tingkatSatu.id_praktikum,
                  nm_praktikum: tingkatSatu.nm_praktikum,
                  id_asisten: tingkatSatu.id_asisten,
                  kls_praktikum: tingkatSatu.kls_praktikum,
                })}
            >
              <Left>
                <Text>{tingkatSatu.id_praktikum}</Text>
              </Left>
              <Right>
                <Ionicons name="ios-arrow-dropright-outline" size={32} />
              </Right>
            </CardItem>
          )}
        />
      : null;

      const tingkatDua = this.renderTingkat2 ();
const CardDua = tingkatDua.length > 0
  ? <Card
      renderHeader={() => (
        <CardItem
          header
          style={{
            backgroundColor: '#08AF9E',
            borderWidth: 0.5,
            borderColor: 'gray',
          }}
        >
          <Text style={{color: 'black'}}> Tingkat 2</Text>
        </CardItem>
      )}
      dataArray={tingkatDua}
      renderRow={tingkatDua => (
        <CardItem
          button
          onPress={() =>
            this.props.navigation.navigate ('Nilai', {
              npm_mhs: tingkatDua.npm_mhs,
              id_praktikum: tingkatDua.id_praktikum,
              nm_praktikum: tingkatDua.nm_praktikum,
              id_asisten: tingkatDua.id_asisten,
              kls_praktikum: tingkatDua.kls_praktikum,
            })}
        >
          <Left>
            <Text>{tingkatDua.id_praktikum}</Text>
          </Left>
          <Right>
            <Ionicons name="ios-arrow-dropright-outline" size={32} />
          </Right>
        </CardItem>
      )}
    />
  : null;

const tingkatTiga = this.renderTingkat3 ();
const CardTiga = tingkatTiga.length > 0
  ? <Card
      renderHeader={() => (
        <CardItem
          header
          style={{
            backgroundColor: '#08AF9E',
            borderWidth: 0.5,
            borderColor: 'gray',
          }}
        >
          <Text style={{color: 'black'}}> Tingkat 3</Text>
        </CardItem>
      )}
      dataArray={tingkatTiga}
      renderRow={tingkatTiga => (
        <CardItem
          button
          onPress={() =>
            this.props.navigation.navigate ('Nilai', {
              npm_mhs: tingkatTiga.npm_mhs,
              id_praktikum: tingkatTiga.id_praktikum,
              nm_praktikum: tingkatTiga.nm_praktikum,
              id_asisten: tingkatTiga.id_asisten,
              kls_praktikum: tingkatTiga.kls_praktikum,
            })}
        >
          <Left>
            <Text>{tingkatTiga.id_praktikum}</Text>
          </Left>
          <Right>
            <Ionicons name="ios-arrow-dropright-outline" size={32} />
          </Right>
        </CardItem>
      )}
    />
  : null;

const TingkatEmpat = this.renderTingkat4 ();
const CardEmpat = TingkatEmpat.length > 0
  ? <Card
      renderHeader={() => (
        <CardItem
          header
          style={{
            backgroundColor: '#08AF9E',
            borderWidth: 0.5,
            borderColor: 'gray',
          }}
        >
          <Text style={{color: 'black'}}> Tingkat 3</Text>
        </CardItem>
      )}
      dataArray={TingkatEmpat}
      renderRow={TingkatEmpat => (
        <CardItem
          button
          onPress={() =>
            this.props.navigation.navigate ('Nilai', {
              npm_mhs: TingkatEmpat.npm_mhs,
              id_praktikum: TingkatEmpat.id_praktikum,
              nm_praktikum: TingkatEmpat.nm_praktikum,
              id_asisten: TingkatEmpat.id_asisten,
              kls_praktikum: TingkatEmpat.kls_praktikum,
            })}
        >
          <Left>
            <Text>{TingkatEmpat.id_praktikum}</Text>
          </Left>
          <Right>
            <Ionicons name="ios-arrow-dropright-outline" size={32} />
          </Right>
        </CardItem>
      )}
    />
  : null;

    return (
      <Container>
        <Content padder>
          <View style={{marginTop:30}}>
            <Image
              source={require ('../../img/images.png')}
              style={{width: '100%', height: 250}}
              resizeMode="contain"
            />
          </View>
            {renderProfile}
          <Card style={{marginTop:15}}>
            <CardItem header bordered style={{ justifyContent: 'center', backgroundColor: '#08AF9E', borderWidth: 0.5, borderColor: 'gray' }}>
              <Text style={{color:'black'}}>Daftar Praktikum</Text>
            </CardItem>
            {CardSatu}
            {CardDua}
            {CardTiga}
            {CardEmpat}
          </Card>  
        </Content>
      </Container>
    );
  }
}
