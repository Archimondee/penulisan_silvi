import ChangePolicy from './ChangePolicy';
import PolicyScreen from './PolicyScreen';
import ResetPolicy from './ResetPolicy';
import UsernamePolicy from './UsernamePolicy';
import WearPolicy from './WearPolicy';
import RulesPolicy from './RulesPolicy';

export {
    ChangePolicy,
    PolicyScreen,
    ResetPolicy,
    UsernamePolicy,
    WearPolicy,
    RulesPolicy
}