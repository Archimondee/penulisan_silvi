import React, {Component} from 'react';
import {
    Container,
    Header,
    Content,
    Form,
    Item,
    Input,
    Label,
    Thumbnail,
    Button,
    Footer
} from 'native-base';
import {StyleSheet, Image, Text, View, KeyboardAvoidingView, Alert, AsyncStorage} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

export default class LoginScreen extends Component{
    constructor (props){
        super(props);
        this.state = {
            UserName: 'prak-52415893',
            UserPassword: 'gilang_ganteng',
        }
    }

    UserLoginFunction = () => {
        const {UserName} = this.state;
        const {UserPassword} = this.state;

        fetch('http://api-labti.000webhostapp.com/api/Login.php',{
            method: 'POST',
            headers: {
                'Accept' : 'application/json',
                'Content-Type' : 'application/json',
            },
            body: JSON.stringify({
                username: UserName,
                password: UserPassword
            })
        }).then((response)=>response.json())
            .then((responseJson)=>{
                if(responseJson === 'Logged In'){
                    this._signInAsync();
                    this.props.navigation.navigate('Profile',{username: UserName});
                }else{
                    Alert.alert(responseJson);
                }
            }).catch((error)=>{
                console.log(error)
            })
    }
    _signInAsync = async () => {
        await AsyncStorage.setItem('username', this.state.UserName);
    }
    render(){
        return(
                <KeyboardAvoidingView style={{width:'100%', height:'100%', backgroundColor:'white'}} behavior="padding" enabled>
                    <Content style={{marginTop:50}}>
                        <Image source={require('../../img/logo.jpg')} style={{width:'100%', height:250}} resizeMode='contain'/>
                        <Form style={{marginTop:40}}>
                            <Item floatingLabel style={{marginLeft:20, marginRight:40}}>
                                <Label>Username</Label>
                                <Input 
                                    onChangeText={UserName => this.setState({UserName})}
                                />
                            </Item>
                            <Item floatingLabel style={{marginLeft:20, marginRight:40}}>
                                <Label>Password</Label>
                                <Input 
                                    onChangeText={UserPassword => this.setState({UserPassword})}
                                    secureTextEntry={true}
                                />
                            </Item>
                            <View style={{alignSelf:'center', flex:1, flexDirection:'row', marginBottom:15}}>
                                <Button 
                                onPress = {this.UserLoginFunction}
                                style={{marginTop:50, height:50, width:100, justifyContent:'center', marginRight:10, backgroundColor:'#08AF9E'}}>
                                    <Text style={{color:'black'}}>Login</Text>
                                </Button>
                                <Button style={{marginTop:50, height:50, width:100, justifyContent:'center', backgroundColor:'#08AF9E'}}>
                                    <Text style={{color:'black'}}>Register</Text>
                                </Button>
                            </View>
                        </Form>
                    </Content>
                </KeyboardAvoidingView>
        );
    }
}