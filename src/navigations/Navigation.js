import {
    createStackNavigator,
    createDrawerNavigator,
    createSwitchNavigator
} from 'react-navigation';
import React from 'react';
import{
    Text,
    Animated,
    Easing
}from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import {
    LoginScreen, 
    ProfileScreen,
    NilaiScreen,
    ResetScreen,
    ChangePolicy, 
    PolicyScreen, 
    ResetPolicy, 
    UsernamePolicy,
    WearPolicy,
    RulesPolicy,
    Auth
} from '../components';
import DrawerContainer from './DrawerContainer';
import {Button} from 'native-base';


 const noTransitionConfig = () => ({
     transitionSpec: {
         duration: 0,
         timing: Animated.timing,
         easing: Easing.step0,
     },
 });

const ProfileStack = createStackNavigator({
    Profile: {screen: ProfileScreen},
    Nilai: {screen: NilaiScreen},
    ResetPassword: {screen: ResetScreen},
},{
        headerMode: 'float',
     navigationOptions: ({navigation}) => ({
         headerStyle: { backgroundColor: '#08AF9E'},
         gesturesEnabled: false,
         headerLeft: (
            //<Ionicons name='ios-menu' size={30} style={{ paddingLeft: 12 }} onPress={() => navigation.navigate('DrawerOpen')} />
             <Button onPress={()=>navigation.openDrawer()} style={{ paddingLeft: 12 }} transparent><Ionicons name='ios-menu' size={30} /></Button>
        ),
     }),
});

const PolicyStack = createStackNavigator({
    Policy: {screen: PolicyScreen},
    Reset: {screen: ResetPolicy},
    Rules: {screen: RulesPolicy},
    Username: {screen: UsernamePolicy},
    Wear: {screen: WearPolicy},
    ChangePassword: {screen: ChangePolicy}
},{
        headerMode: 'float',
     navigationOptions: ({navigation}) => ({
         headerStyle: { backgroundColor: '#08AF9E'},
         gesturesEnabled: false,
         headerLeft: (
             <Button onPress={() => navigation.openDrawer()} style={{ paddingLeft: 12 }} transparent><Ionicons name='ios-menu' size={30} /></Button>
        ),
     }),
});

const DrawerStack = createDrawerNavigator({
    ProfileDrawer: {screen: ProfileStack},
    PolicyDrawer: {screen: PolicyStack}
},{
    gesturesEnabled: false,
    contentComponent: props => <DrawerContainer {...props} />,
});

export default Navigation = createSwitchNavigator({
    AuthLoading: {screen:Auth},
    loginStack: {screen: LoginScreen},
    drawerStack: {screen: DrawerStack},
},{
    headerMode: 'none',
    title: 'Main',
    initialRouteName: 'AuthLoading',
    transitionConfig: noTransitionConfig,
})