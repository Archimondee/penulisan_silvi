import React, { Component } from 'react';
import {  View, Text, StyleSheet, Image, AsyncStorage} from 'react-native';
import {NavigationActions} from 'react-navigation';
import {Container, Content, Card, CardItem, Body, Button, Right, Left, Footer} from 'native-base';
import {Ionicons, Feather} from '@expo/vector-icons';

export default class DrawerContainer extends Component {
    constructor(props){
        super(props);
        this.state = {
            dataDiri : []
        }
    }
    _getData(){
        AsyncStorage.getItem('Profile').then((value)=>{
            var hasil = JSON.parse(value);
            this.setState({
                dataDiri: hasil
            })
        })
    }
    _signOutAsync = async ()=>{
      await AsyncStorage.clear();
      this.props.navigation.navigate('loginStack');
    }
    // _renderProfile(){
    //     var data = this.state.dataDiri;
    //     console.log(data)
    //     return(
    //         <Body style={{justifyContent:'center', alignContent:'center', alignItems:'center', alignSelf:'center'}}>
    //             <Text>Nama : {this.state.dataDiri.nama_mhs}</Text>
    //             <Text>Kelas : {this.state.dataDiri.kls_praktikum}</Text>
    //             <Text>NPM : {this.state.dataDiri.npm_mhs}</Text>
    //         </Body>
    //     )
    // }
    componentWillUpdate(){
        this._getData();
    }
    render() {
      const {navigation} = this.props;
      
    return (
        <Container>
            <Content padder>
                <View style={{}}>
                    <View style={{ marginTop:30, marginBottom:5 }}>
                        <Image
                            source={require('../img/images.png')}
                            style={{ width: '100%', height: 150 }}
                            resizeMode="contain"
                        />
                        <Card style={{paddingLeft:5, paddingRight:5}}>
                            <CardItem>
                                {
                                    this.state.dataDiri.map((items, i)=>{
                                        return(
                                            <Body key={i} style={{justifyContent:'center', alignContent:'center', alignItems:'center', alignSelf:'center'}}>
                                                <Text>Nama : {items.nama_mhs}</Text>
                                                <Text>Kelas : {items.kls_praktikum}</Text>
                                                <Text>NPM : {items.npm_mhs}</Text>
                                             </Body>
                                        )
                                    })
                                }
                            </CardItem>
                        </Card>
                    </View>
                    <View style={{marginTop:25}}>
                        <Button style={{ width: '100%', backgroundColor:'#08AF9E'}} onPress={()=>navigation.navigate('Profile')}>
                            <View style={{margin:5}}>
                                <Ionicons name="md-person" size={32} />
                            </View>
                            <Left style={{margin:5}}>
                                <Text style={{fontSize:18}}>Profile</Text>
                            </Left>
                        </Button>
                        <Button style={{ width: '100%', backgroundColor: '#08AF9E', marginTop: 5 }} onPress={() => navigation.navigate('Policy')}>
                            <View style={{ margin: 5 }}>
                                <Ionicons name="ios-archive" size={32} />
                            </View>
                            <Left style={{ margin: 5 }}>
                                <Text style={{ fontSize: 18 }}>Policy</Text>
                            </Left>
                        </Button>
                        <Button style={{ width: '100%', backgroundColor: '#08AF9E', marginTop: 5 }} onPress={this._signOutAsync}>
                            <View style={{ margin: 5 }}>
                                <Feather name="log-out" size={32} />
                            </View>
                            <Left style={{ margin: 5 }}>
                                <Text style={{ fontSize: 18 }}>Logout</Text>
                            </Left>
                        </Button>
                    </View>
            </View>
                
                
            </Content>
            <Footer style={{width:'100%',backgroundColor:'white', flexDirection:'column', justifyContent:'center', alignContent:'center', alignItems:'center', alignSelf:'center'}}>
                <Text>Laboratorium Teknik Informatika</Text>
                <Text>Universitas Gunadarma</Text>
                <Text>2018</Text>
            </Footer>
        </Container>
        
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ccc',
        paddingTop: 40,
        paddingHorizontal: 20,
    },
    uglyDrawerItem: {
        fontSize: 20,
        color: 'blue',
        padding: 15,
        margin: 5,
        borderRadius: 10,
        borderColor: 'blue',
        borderWidth: 1,
        textAlign: 'center',
        backgroundColor: 'white',
        overflow: 'hidden',
    },
});

